const express = require('express');
const puppeteer = require('puppeteer');

const app = express();
const port = 3000;

app.use(express.json());

app.post('/html-to-image', async (req, res) => {
    const {htmlContent} = req.body;

    try {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.setContent(htmlContent);

        const screenshot = await page.screenshot({fullPage: true, type: 'png'});

        await browser.close();

        res.set('Content-Type', 'image/png');
        res.send(screenshot);
    } catch (error) {
        console.error(error);
        res.status(500).send('Error taking screenshot');
    }
});

app.post('/url-to-image', async (req, res) => {
    const {url} = req.body;
    const {width, height} = req.body;

    if (!url) {
        return res.status(422).send('URL is required');
    }

    try {
        const browser = await puppeteer.launch({headless: true});
        const page = await browser.newPage();
        await page.goto(url, {waitUntil: 'networkidle2'});
        let screenshot = null
        if (width && height) {
            await page.setViewport({width: width, height: height});
            screenshot = await page.screenshot({
                clip: {
                    x: 0,
                    y: 0,
                    width: width,
                    height: height
                }
            });
        } else {
            screenshot = await page.screenshot({fullPage: true});
        }

        await browser.close();

        res.setHeader('Content-Type', 'image/png');

        res.send(screenshot);

    } catch (error) {
        console.error(error);
        res.status(500).send('Error taking screenshot');
    }
})

app.listen(port, () => {
    console.log(`Puppeteer service listening at http://localhost:${port}`);
});
